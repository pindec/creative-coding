# creative coding

## Creative coding resources using Processing

# Aim
A series of resources to support creative coding workshops for non-coders

# Pre-requisites
* Some learners
* Processing

# Outline plan (~1.5hr workshop)
* Talk briefly but enthusiastically at the start about why you love coding, show a few simple inspiring examples (maybe something they are going to program themselves, plus something you’ve made. 

    * For inspring under-represented groups to code, I’d also mention/show some more role model/s briefly (especially if the role models have been involved in related work). But not too much talking! Hands dirty fast :).

* Proivde a series of practical hands-on tasks that they can replicate/build on at home. I use [Processing](https://processing.org) but you might prefer [Scratch](https://scratch.mit.edu)

* Integrate the students in the tasks directly (e.g. they upload an image of themselves and then gltich it in code; use the webcam; find their own image to use in a game)

* Emphasise that it’s OK to get it wrong. I tend to show them my code, then make a mistake / enter some crazy values / break it in some way, and show them how to fix it. 

    * Encourage them to break things in interesting ways. 
	
	* Have a printed set of support docs by each computer, including simple FAQs.

* Cater for lots of levels. For each task, I make a basic starting example that anyone should be able to run just with copy and paste, but suggest multiple increasingly difficult extensions (including open-ended ones) for those who the task clicks with.

* Get them to work in small groups (ideally 2 people) so they can pair-program (and mention that it’s a real thing in coding jobs)

* Have some extra helpers who go round and engage directly with students, instead of just one person talking from the front. If that’s not possible, make sure you go round and engage one-on-one. Helpers shouldn’t take over the students’ keyboard/mouse or stand over them – get physically on their level and ask them to do the keyboard-driving.

* Give students a take-home resource so that they can continue at home (e.g. links to resources, Processing downloaded on a USB drive, copy of their game/whatever).

* Do a test run if possible to generate FAQs and a rough idea of how long each task might take. Preferably with target audience but any non-coders in your organisation will do.

* Rough structure
  
    * Starter / early bird task open and ready for people to start playing with when they arrive – deals with the awkward waiting-for-something-to-happen bits.

    * Brief intro to outline what you’re going to do and what they will be doing
  
    * Series of tasks every 10 mins or so so that you can move on – gives people that didn’t like a particular exercise something else to do! (but if a group wants to carry on with a previous task that’s also fine :))
    encourage them to share, but don’t force them, at the task changovers. I try to highlight to the group the interesting things I’d seen people do, but don’t force them to stand up and talk.
        brief overall wrap up at the end.


# Code
* coming soon